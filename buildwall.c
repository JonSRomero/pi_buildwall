// first OpenVG program
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "VG/openvg.h"
#include "VG/vgu.h"
#include "fontinfo.h"
#include "shapes.h"
#include "jsmn.h"
#include <stdio.h>

#define bool char
#define true (1)
#define false (0)
#define NUM_JSMN_TOKENS 1000
#define NUM_CELLS (100)

const int min_characters = 30;
int cellCount;
char** cellStrings;
  
typedef struct color {
  int r, g, b;
} colorType;

colorType yellow;
colorType red;
colorType green;
colorType blue;

/**
 * Read an ASCII file into a dynamically allocated string buffer.
 *
 * @param fileName The name of the file to read.
 *
 * @return A string containing the file read. The string is allocated with 
 *   malloc; the caller is responsible for disposal.
 */
char* readAsciiFile( char* fileName ) {
  const int buffIncrement = 512;
  size_t buffSize = 0;
  char* buffer = NULL;
  size_t fileSize = 0;

  FILE* f = fopen(fileName, "r");
  while (!feof(f)) {
    if ( buffSize == 0 ) {
      buffer = malloc( buffIncrement + 1 );
      buffSize = buffIncrement;
    } else {
      char* buffer2 = malloc( buffSize + buffIncrement + 1 );
      memcpy( buffer2, buffer, buffSize );
      free(buffer);
      buffer = buffer2;
      buffSize += buffIncrement;
    }
    fileSize += fread( buffer + fileSize, 1, buffIncrement, f);
  }
  buffer[ fileSize ] = (char) 0;

  char* retValue = malloc( fileSize + 1 );
  strcpy( retValue, buffer );
  free( buffer );
  return retValue;
}

void drawCell( VGfloat x, VGfloat y, VGfloat width, VGfloat height, colorType* color, char *text ) {
  VGfloat radius = (width + height) / 20;

  Fill( color->r, color->g, color->b, 1.0 );
  Roundrect( x, y, width, height, radius, radius);
  Fill( 0, 0, 0, 1.0 );
  int font_size = width / (strlen(text) > min_characters ? strlen(text) : min_characters);

  TextMid( x + width / 2, y + height / 2, text, SerifTypeface, font_size );
}

void draw ( int width, int height ) {
  const int horizontalCells = 3;
  const int verticalCells = 6;
  const int space = 10;

  Image(0, 0, 1920, 1080, "images/background.jpg"); 

  int cellWidth = ( width - space * (horizontalCells - 1) ) / horizontalCells;
  int cellHeight = ( height - space * (verticalCells - 1) ) / verticalCells;

  int hLoc = 0;
  int h;
 
 for ( h = 0; h < horizontalCells; ++h) {
    int vLoc = 0;
    int v;
    for ( v = 0; v < verticalCells; ++v) {
      drawCell( hLoc, 
		    vLoc, 
		    cellWidth, 
		    cellHeight,
		    h == 0 && v == 0 ? &yellow : &red, 
		    v < cellCount ? cellStrings[v] : "" );
      vLoc += cellHeight + space;
    }
    hLoc += cellWidth + space;
  }
  End();
}

void init_colors() {

	yellow.r = 255;
	yellow.g = 255;
	yellow.b = 0;

	red.r = 255;
	red.g = 0;
	red.b = 0;

	green.r = 0;
	green.g = 255;
	green.b = 0;

	blue.r = 0;
	blue.g = 0;
	blue.b = 255;

}



int main() {

  char *json_text = readAsciiFile("/home/pi/buildwall_json.txt");

  int width, height;
  init(&width, &height);				   // Graphics initialization

  init_colors();

  jsmn_parser parser;
  jsmntok_t tokens[NUM_JSMN_TOKENS];

  jsmn_init(&parser);

  int num_tokens = jsmn_parse(&parser, json_text, strlen(json_text), tokens, NUM_JSMN_TOKENS);

  int i;

  int tokenIndex = 0;
  bool done = false;
  cellCount = 0;
  cellStrings = malloc(sizeof(char *)*NUM_CELLS);
  while (!done) {
    while (tokenIndex < num_tokens && tokens[tokenIndex].type != JSMN_ARRAY) {
      ++tokenIndex;
    }
    if (tokenIndex < num_tokens) {
      int numArrayElements = tokens[tokenIndex].size;
      ++tokenIndex;
      int k;
      while (tokenIndex + 7 < num_tokens && numArrayElements--) {
        size_t length = tokens[tokenIndex+2].end - tokens[tokenIndex+2].start;
        cellStrings[cellCount] = malloc(length + 1);
        strncpy(cellStrings[cellCount], json_text + tokens[tokenIndex+2].start, length);
        cellStrings[cellCount][length] = (char) 0;
        ++cellCount;
        printf("%s\n", cellStrings[cellCount-1]);
        tokenIndex += 7; 
      }
      putc('\n', stdout);
    } else {
      done = true;
    }
  }
   
  Start(width, height);				   // Start the picture
	
  while (1) {
    draw(width, height);
  }
}
/* vim:  set tabstop=2:shiftwidth=2 */
