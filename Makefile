INCLUDEFLAGS=-I/opt/vc/include -I/opt/vc/include/interface/vmcs_host/linux -I/opt/vc/include/interface/vcos/pthreads -I../openvg/ -I../jsmn/
LIBFLAGS=-L/opt/vc/lib -L../jsmn -lEGL -lGLESv2 -lbcm_host -lpthread  -ljpeg -ljsmn

all: buildwall

buildwall:	buildwall.c ../openvg/libshapes.o ../openvg/oglinit.o
	gcc -Wall $(INCLUDEFLAGS) -o  buildwall buildwall.c ../openvg/libshapes.o ../openvg/oglinit.o $(LIBFLAGS)

